import 'dart:io';

void main() {
  //soal nomor 1
  print("LOOPING PERTAMA");
  var angka = 2;
  while (angka <= 20) {
    print("$angka - I  love coding");
    angka += 2;
  }
  print("LOOPING KEDUA");
  var number = 20;
  while (number >= 2) {
    print("$number - I will become a mobile developer");
    number -= 2;
  }

  //soal nomor 2
  for (var i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 != 0) {
      print("$i - I Love Coding");
    } else if (i % 2 != 0) {
      print("$i - Santai");
    } else {
      print("$i - Berkualitas");
    }
  }

  //soal nomor 3
  for (int a = 0; a < 4; a++) {
    for (int b = 0; b < 8; b++) {
      stdout.write("#");
    }
    print(" ");
  }
  // for (var a = 1; a <= 4; a++) {
  //   print("########");
  // }

  //soal nomor 4
  for (int j = 0; j < 7; j++) {
    for (int k = 0; k <= j; k++) {
      stdout.write("#");
    }
    print(" ");
  }
}
