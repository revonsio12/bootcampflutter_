void main() {
  //output nomor 1
  print(teriak());

  //output nomor 2
  var num1 = 12;
  var num2 = 4;
  var hasilkali = kalikan(num1, num2);
  print(hasilkali);

  //output nomor 3
  var name = "Agus";
  var age = 30;
  var address = "Jln. Malioboro, Yogyakarta";
  var hobby = "Gaming";

  var perkenalan = introduce(name, age, address, hobby);
  print(perkenalan);

  //output nomor 4
  var angka = 6;
  print("$angka! = " + faktorial(angka).toString());
}

//function
//soal nomor 1
teriak() {
  return "Halo Sanbers!";
}

//soal nomor 2
kalikan(num1, num2) {
  return num1 * num2;
}

//soal nomor 3
introduce(name, age, address, hobby) {
  return "Nama saya $name, umur saya $age tahun, alamat saya di $address, dan saya punya hobby yaitu $hobby!";
}

//soal nomor 3
faktorial(angka) {
  if (angka == 0 || angka == 1) {
    return 1;
  } else if (angka < 0) {
    return "Tidak Terdefinisi";
  } else {
    return angka * faktorial(angka - 1);
  }
}
