class Lingkaran {
  late double phi;
  late double r;

  void setPhi(double value) {
    if (value < 0) {
      value *= -1;
    }
    phi = value;
  }

  double getPhi() {
    return phi;
  }

  void setr(double value) {
    if (value < 0) {
      value *= -1;
    }
    r = value;
  }

  double getr() {
    return r;
  }

  double hitungLuas() {
    return this.phi * this.r * this.r;
  }
}
