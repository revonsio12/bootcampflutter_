import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(List<String> args) {
  armor_titan a = armor_titan();
  attack_titan b = attack_titan();
  beast_titan c = beast_titan();
  human d = human();

  a.powerPoint = 5;
  b.powerPoint = 5;
  c.powerPoint = 5;
  d.powerPoint = 5;

  //powerPoint
  print('Power point dari armor titan a = ${a.powerPoint}');
  print('Power point dari attack titan b = ${b.powerPoint}');
  print('Power point dari beast titan c = ${c.powerPoint}');
  print('Power point dari human d = ${d.powerPoint}');

  //object string
  print('SIfat dari armor titan a = ${a.terjang()}');
  print('SIfat dari attack titan b = ${b.punch()}');
  print('SIfat dari beast titan c = ${c.lempar()}');
  print('SIfat dari human titan d = ${d.killAlltitan()}');
}
