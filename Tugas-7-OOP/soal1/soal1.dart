class segitiga {
  late double setengah;
  late double alas;
  late double tinggi;

  double hitung_luas() {
    return setengah * this.alas * this.tinggi;
  }
}

void main(List<String> args) {
  var luas_segitiga;
  segitiga triangle;

  triangle = new segitiga();
  triangle.setengah = 0.5;
  triangle.alas = 20.0;
  triangle.tinggi = 30.0;

  luas_segitiga = triangle.hitung_luas();

  print(luas_segitiga);
}
