import 'bangun_datar.dart';

class segitiga extends bangun_datar {
  late double alas;
  late double tinggi;
  late double setengah;
  late double sisi_miring;

  segitiga(double alas, double tinggi, double setengah, double sisi_miring) {
    this.alas = alas;
    this.tinggi = tinggi;
    this.setengah = setengah;
    this.sisi_miring = sisi_miring;
  }

  @override
  double luas() {
    return alas * tinggi * setengah;
  }

  @override
  double keliling() {
    return alas + tinggi + sisi_miring;
  }
}
