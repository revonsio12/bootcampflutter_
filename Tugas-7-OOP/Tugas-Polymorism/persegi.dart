import 'bangun_datar.dart';

class persegi extends bangun_datar {
  late double s;

  persegi(double s) {
    this.s = s;
  }

  @override
  double luas() {
    return s * s;
  }

  @override
  double keliling() {
    return s * 2;
  }
}
