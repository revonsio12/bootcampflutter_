import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main(List<String> args) {
  bangun_datar bangunDatar = new bangun_datar();
  lingkaran bulat = new lingkaran(7, 3.14);
  persegi kotak = new persegi(4);
  segitiga siku = new segitiga(2, 3, 0.5, 5);

  bangunDatar.luas();
  bangunDatar.keliling();

  print('Luas lingkaran: ${bulat.luas()}');
  print('Keliling lingkaran: ${bulat.keliling()}');

  print('Luas persegi: ${kotak.luas()}');
  print('Keliling persegi: ${kotak.keliling()}');

  print('Luas segitiga: ${siku.luas()}');
  print('Keliling segitiga: ${siku.keliling()}');
}
