void main() {
  //output nomor 1
  print(range(10, 1));

  //output nomor 2
  print(rangeWithStep(10, 1, 2));

  //output nomor 3
  dataHandling(input);

  // //output nomor 4
  balikkata('katak bhizer');
}

//function nomor 1
range(startNum, finishNum) {
  List<int> angka = [];
  if (startNum > finishNum) {
    var rangeLength = startNum - finishNum + 1;
    for (var i = 0; i < rangeLength; i++) {
      angka.add(startNum - i);
    }
    return angka;
  } else {
    var rangeLength = finishNum - startNum + 1;
    for (var i = 0; i < rangeLength; i++) {
      angka.add(startNum + i);
    }
    return angka;
  }
}

//function nomor 2
rangeWithStep(startNum, finishNum, step) {
  List<int> angka = [];
  if (startNum > finishNum) {
    var currentNum = startNum;
    for (var i = 0; currentNum >= finishNum; i++) {
      angka.add(currentNum);
      currentNum -= step;
    }
  } else {
    var currentNum = startNum;
    for (var i = 0; currentNum <= finishNum; i++) {
      angka.add(currentNum);
      currentNum += step;
    }
  }
  return angka;
}

//function nomor 3
var input = [
  ['0001', 'Roman Alamsyah', 'Bandar Lampung', '21/05/1989', 'Membaca'],
  ['0002', 'Dika Sembiring', 'Medan', '10/10/1992', 'Bermain Gitar'],
  ['0003', 'Winona', 'Ambon', '25/12/1965', 'Memasak'],
  ['0004', 'Bintang Sanjaya', 'Martapura', '6/4/1970', 'Berkebun']
];
dataHandling(data) {
  var dataLength = data.length;
  for (var i = 0; i < dataLength; i++) {
    var id = "Nomor ID: " + data[1][0];
    var nama = "Nama Lengkap: " + data[1][1];
    var ttl = "TTL: " + data[1][2] + data[1][3];
    var hobi = "Hobi: " + data[1][4];

    print(id);
    print(nama);
    print(ttl);
    print(hobi);
    print('');
  }
}

//function nomor 4
balikkata(str) {
  List<String> newStr = [];
  dynamic index = (str.length);
  while (index > 0) {
    index -= 1;
    newStr.add(str[index]);
  }
  print(newStr.join(''));
}
